/* eslint no-console: "off"*/

import path from 'path';
import { Server } from 'http';
import Express from 'express';

import es from 'elasticsearch';
import BuildBody from './Utils/BuildBody';
import ElasticRequest from './Utils/ElasticRequest';
import GetAll from './Utils/GetAll';
const app = new Express();
const server = new Server(app);
const client = new es.Client({
    host: 'localhost:9200',
    log: [{
        type: 'stdio',
        levels: ['error']
    }]
});
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.get('/', (req, res, next) => {
    const body = BuildBody(req);
    ElasticRequest(client, body,res);
});
app.get('/all', (req, res, next) => {
    GetAll(client,res)
});
const port = 3001;
const env = 'production';
process.env.UNIVERSAL = false;

server.listen(port, (err) => {
    if (err) {
        return console.error(err);
    }
    return console.info(
        `
      Server running on http://localhost:${port} [${env}]
      Universal rendering: ${process.env.UNIVERSAL ? 'enabled' : 'disabled'}
    `);
});




