import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { App } from './components/App';

export class AppClient extends React.Component {
  constructor(){
    super();
  }
  render() {
    return (
    <Router>
      <App />
    </Router>);
  }
}

window.onload = () => {
  render(<AppClient />, document.getElementById('main'));
};
