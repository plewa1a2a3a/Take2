export default function (req) {

    const body = {
        //size: 2
    };

    function needQuery() {
        if (body.query === undefined) {
            body.query = { bool: { must: [] } }
        }
    }

    if (req.query.page !== undefined) {
        body.from = 5 * 1;
    }


    if (req.query.category !== undefined && req.query.category !== "") {
        needQuery();
        body.query.bool.must.push({ match: { category: req.query.category } });
    }

    if (req.query.search !== undefined && req.query.search !== "") {
        needQuery();
        body.query.bool.must.push({
            multi_match: {
                query: req.query.search,
                fields: ["model", "brand", "features"]
            }
        });
    }
    if (req.query.search == "") {
        needQuery();
        body.query.bool.must.push({
            match_all: {}
        });
    }


    if (req.query.minPrice !== undefined && req.query.minPrice !== "") {
        needQuery();
        body.query.bool.must.push({ range: { price: { gte: parseFloat(req.query.minPrice) } } });
    }

    if (req.query.maxPrice !== undefined && req.query.maxPrice !== "") {
        needQuery();
        body.query.bool.must.push({ range: { price: { lte: parseFloat(req.query.maxPrice) } } });
    }

    if (req.query.order !== undefined && req.query.order !== "") {
        switch (req.query.order) {
            case "None":
                break;
            case "price_asc":
                body.sort = [
                    { "price": "asc" }
                ];
                break;
            case "price_desc":
                body.sort = [
                    { "price": "desc" }
                ];
                break;
            case "rate_asc":
                body.sort = [
                    { "rate": "asc" }
                ];
                break;
            case "rate_desc":
                body.sort = [
                    { "rate": "desc" }
                ];
                break;
            case "name_asc":
                body.sort = [
                    { "brand": "asc" }
                ];
                break;
            default:
                next(new Error(`Unknwon order: ${req.query.order}`));
                return;
        }
    }

    return body;
}