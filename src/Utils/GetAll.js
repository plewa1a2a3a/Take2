export default function (client, res) {

    client.search({
        index: 'newshop',
        type: 'product',
        body: {
            query: {
                match_all: {}
            }
        }
    }, (error, response, status) => {
        var myHits = [];
        if (error) {
            res.send(error)
        }
        else {
            response.hits.hits.forEach(function (hit) {
                myHits.push(hit._source);
            })
            var obj = {
                products: myHits,
                pages: Math.ceil(response.hits.total / 5)
            }
            res.send(obj);
        }
    });
}

