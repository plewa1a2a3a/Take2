import React from 'react';
import RecomendedProduct from './RecomendedProduct'
import axios from 'axios';


export default class IndexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {products : 'empty'};
    }
    componentWillMount() {
        this.myAjax();
    }
    myAjax() {
        var me = this;
        axios.get('http://localhost:3001/all', {
            params: {
                category: "cameras"
            }
        })
            .then(function (response) {
                me.setState({ products: response.data.products });

            })
            .catch(function (error) {
                console.log(error);
            });
    }
    renderRecomended() {
        if (this.state.products == 'empty') return;
        var products = [];
        for (let i = 0; i < 4; i++) {
            products.push(this.renderProduct(this.state.products[i], i))
        }
        return products;

    }
    renderProduct(aProduct, key){
        return(
            <RecomendedProduct product={aProduct} key={key}/>
        )
    }
    render() {
        return (
            <div className="indexPage">
                <h1> Witaj Janusz w SpaceMarket</h1>
                <h4> Polecane produkty </h4>
                <div className="recomendedProducsts">
                    {this.renderRecomended()}
                </div>
            </div>
        );
    }
}