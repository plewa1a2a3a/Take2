import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Layout } from './Layout';
import { NotFoundPage } from './NotFoundPage';
import  IndexPage  from './IndexPage'
import { Content } from './Content'
export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { products: {} }

  }
  render() {
    return (
      <Layout refresh={this.refresh} props={this.props}>
        <Switch>
          <Route exact path="/" render={(props) => <IndexPage {...props} category={"cameras"} products={this.state.products} />} />
          <Route exact path="/products/:category?" render={(props) => <Content {...props} category={"cameras"} products={this.state.products} />} />
          <Route component={NotFoundPage} />
        </Switch>
      </Layout>
    )
  }
}




