/* eslint react/forbid-prop-types: "off" */

import React from 'react';
export class NotFoundPage extends React.Component {
  componentWillMount() {
    const { staticContext } = this.props;
    if (staticContext) {
      staticContext.is404 = true;
    }
  }

  render() {
    
    return (<div className="not-found">
      <h1>404</h1>
      <h2>Page not found! <code>{this.props.location.pathname}</code></h2>
      <p>
      </p>
    </div>
    );
  }
}

export default NotFoundPage;
