import React from 'react';
import Product from './Product';
import axios from 'axios'

export default class RecomendedProduct extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="recoemendedProduct">
                <h1>{this.props.product.brand}</h1>
                <h3>{this.props.product.model}</h3>
                <p>{this.props.product.price}zł</p>
                 <img src={`./../img/${this.props.product.brand} ${this.props.product.model}.png`} />
            </div>
        )
    }
}


