import React from 'react';
import Search from './Search';
import Products from './Products';


import axios from 'axios'
import queryString from 'query-string'

export class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = { products: {} };
        this.myAjax = this.myAjax.bind(this);
        this.paramUpdater = this.paramUpdater.bind(this);
    }
    // componentDidMount() {
    //     switch (location.pathname) {
    //         case "/products/cameras": {
    //             this.myAjax({ category: "cameras" });
    //             break;
    //         }
    //         case "/products/laptops": {
    //             this.myAjax({ category: "laptops" });
    //             break;
    //         }
    //         case "/products/smartphones": {
    //             this.myAjax({ category: "smartphones" });
    //             break;
    //         }
    //         default: {
    //             break;
    //         }
    //     }
    // }
    // componentWillReceiveProps() {
    //     switch (location.pathname) {
    //         case "/products/cameras": {
    //             this.myAjax({ category: "cameras" });
    //             break;
    //         }
    //         case "/products/laptops": {
    //             this.myAjax({ category: "laptops" });
    //             break;
    //         }
    //         case "/products/smartphones": {
    //             this.myAjax({ category: "smartphones" });
    //             break;
    //         }
    //         default: {
    //             break;
    //         }
    //     }

    // }
    componentWillMount(){
        this.myAjax();
    }

    paramUpdater(param, value) {
        const parsed = queryString.parse(location.search);
        parsed[param] = value;
        history.replaceState(null, null, location.protocol + '//' + location.host + location.pathname + "?" + queryString.stringify(parsed));
        this.setState({ [param]: value });
    }
    myAjax() {
        var me = this;
        const parsed = queryString.parse(location.search);
        var str = location.pathname;
        var n = str.lastIndexOf('/');
        var category = str.substring(n + 1);
        axios.get('http://localhost:3001/', {
            params: {
                search: parsed.search ? parsed.search : "",
                category: parsed.category ? parsed.category : category,
                minPrice: parsed.minPrice ? parsed.category : 0,
                maxPrice: parsed.maxPrice ? parsed.maxPrice : 100000,
                minRate: parsed.minRate ? parsed.minRate : 0
            }
        })
            .then(function (response) {
                console.log(response.data.products);
                me.setState({ products: response.data.products, pages: response.data.pages });

            })
            .catch(function (error) {
                console.log(error);
            });
    }
    render() {
        console.log("content rendered")
        return (
            <div className="content">
                <Search paramUpdater={this.paramUpdater} ajaxFunction={this.myAjax} />
                <Products myAjax={this.myAjax} products={this.state.products} pages={this.state.pages} />

            </div>
        )
    }
}