import React from 'react';



export default class Rating extends React.Component {
    render() {
        return (
            <div className='rating'>
                <h3> Ocena produktu: {this.props.rating} / 5 ({Math.floor(Math.random()*20)} ocen) </h3>
                <img className="stars" style={this.countMask(this.props.rating)} src={'./../img/star.png'} />
            </div>
        )
    }
    countMask(rating){
        var percentage = rating * 2 * 10;
        percentage = 100 - percentage;
        
        return {clipPath :`inset(0px ${percentage}% 0px 0%)`}
    }
}