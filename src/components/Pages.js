import React from 'react';



export default class Pages extends React.Component {
    constructor(){
        super();
        //this.clickHandle = this.clickHandle.bind(this);
    }
    clickHandle(nr){
        this.props.setPage(nr);
    }
    renderPage(nr) {
        return (
            <li onClick={this.clickHandle.bind(this, nr)} key={nr} ><a href="#">{nr}</a></li>
        )
    }
    renderPages(count) {
        var arr = [];
        for (let i = 1; i < count + 1; i++) {
            arr.push(this.renderPage(i));
        }
        return arr;
        
    }
    render() {
        console.log("RENDER PAGES", this.props.pages)
        return (
            <ul className="pagination">
                {this.renderPages(this.props.pages)}
            </ul>
        )
    }
}