import React from 'react';
import { Route, Link } from 'react-router-dom';
import Search from './Search';
import Products from './Products';

export class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <header>
          <h1 className="logo">SPACE MARKET</h1>
          <div className="navBar">
            <ul className="navList">

              <Route path={'/'}>
                <li><Link to={"/"}>Strona glowna</Link></li>
              </Route>

              <Route path={'/prducts/:category?'}>
                <li><Link to={"/products/cameras"}>Aparaty</Link></li>
              </Route>

              <Route path={'/prducts/:category?'}>
                <li><Link to={"/products/laptops"}>Laptopy</Link></li>
              </Route>

              <Route path={'/prducts/:category?'}>
                <li><Link to={"/products/smartphones"}>Smartfony</Link></li>
              </Route>

            </ul>
          </div>
        </header >
        {this.props.children}
      </div >

    )
  }
}