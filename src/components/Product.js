import React from 'react';
import Rating from './Rate';
import $ from "jquery";


export default class Product extends React.Component {
    constructor() {
        super();
    }
    features() {
        this.props.product.featuresList =
            this.props.product.features.map((feature) => {
                return (
                    <li key={feature.toString()}>
                        <a>{feature}</a>
                    </li>)
            });
        return this.props.product.featuresList;

    }
    getImgSize(imgSrc) {
        var newImg = new Image();
        newImg.src = imgSrc;
        var height = newImg.height;
        var width = newImg.width;
        var p = $(newImg).ready(function () {
            return { width: newImg.width, height: newImg.height };
        });
        return { x: p[0]['width'], y: p[0]['height'] };
    }
    render() {
        var style = {
            height: 300//this.getImgSize(require(`./../img/${this.props.product.brand} ${this.props.product.model}.png`)).y + 51
        }
        if (style.height < 230) {
            style.height += 50;
        }

        return (
            <div style={style} className="product">
                <a className="model">{this.props.product.brand} {this.props.product.model}</a> < br />
                <a className="price">{this.props.product.price}zł</a> <br />
                <ul>{this.features()}</ul>
                <form className='buyNowForm'>
                    <input type="submit" value="Kup teraz!" />
                </form>
                <Rating rating={this.props.product.rate} />
                <img src={`./../img/${this.props.product.brand} ${this.props.product.model}.png`} />

            </div>
        )
    }
}
