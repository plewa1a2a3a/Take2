import React from 'react';
import axios from 'axios'


export default class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            minPrice: 0,
            maxPrice: 0,
            minRate: 0,
            category: "cameras"
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.handleMinPrice = this.handleMinPrice.bind(this);
        this.handleMaxPrice = this.handleMaxPrice.bind(this);
        this.handleMinRate = this.handleMinRate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleOrder = this.handleOrder.bind(this);

    }
    handleSubmit() {
        this.props.ajaxFunction();
    }
    handleSearch(event) {
        this.setState({ search: event.target.value });
        console.log(this.state.search, " ==============> Search state search")
        this.props.paramUpdater("search", event.target.value);
        this.state = { search: event.target.value };
        this.handleSubmit();
    }
    handleCategory(event) {
        this.setState({ category: event.target.value });
        this.props.paramUpdater("category", event.target.value);
    }
    handleMinPrice(event) {
        this.setState({ minPrice: event.target.value });
        this.props.paramUpdater("minPrice", event.target.value)
    }
    handleMaxPrice(event) {
        this.setState({ maxPrice: event.target.value });
        this.props.paramUpdater("maxPrice", event.target.value)
    }
    handleMinRate(event) {
        this.setState({ minRate: event.target.value });
        this.props.paramUpdater("minRate", event.target.value);
    }
    handleOrder(event) {
        this.setState({ minRate: event.target.value });
        this.props.paramUpdater("order", event.target.value);
    }
    handleKeyDown(e, cb) {
        if (e.key === 'Enter' && e.shiftKey === false) {
            e.preventDefault();
            cb();
        }
    }
    render() {
        return (
            <form className="search"
                onKeyDown={(e) => { this.handleKeyDown(e, this.handleSubmit); }} >
                <label>
                    <input name="search" type="text" onChange={this.handleSearch} placeholder="Filtruj po nazwie" />
                </label>
                <select name="category" onChange={this.handleCategory} className="categoryFilter">
                    <option value="cameras">Aparaty</option>
                    <option value="laptops">Laptopy</option>
                    <option value="smartphones">Smartfony</option>
                </select>
                <input name="minPrice" onChange={this.handleMinPrice} className="minPriceInput" type="number" min="0" max="15000" placeholder="cena minimalna 00,00 zl" />
                <input name="maxPrice" onChange={this.handleMaxPrice} className="maxPriceInput" type="number" min="0" max="15000" placeholder="cena maxymalna 00,00 zl" />

                <select name="minRate" onChange={this.handleMinRate} className="categoryFilter">
                    <option defaultValue="selected" disabled="disabled">Minimalna ocena</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                <select name="order" onChange={this.handleOrder} className="categoryFilter">
                    <option defaultValue="selected" disabled="disabled">Sortuj wedlug</option>
                    <option value="name_asc" >Alfabetycznie</option>
                    <option value="price_asc" >Cena rosnaco</option>
                    <option value="price_desc">Cena malejaco</option>
                    <option value="rate_asc">Ocena rosnaco</option>
                    <option value="rate_desc">Ocena malejaco</option>
                </select>

                {/*<button type="button" onClick={this.handleSubmit} >Szukaj<i className="margin-left glyphicon glyphicon-search"></i></button>*/}
                <button type="button" onClick={this.handleSubmit} value="szukaj" />
            </form>
        );
    }
}
