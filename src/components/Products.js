import React from 'react';
import Product from './Product';
import Pages from './Pages';

export default class Products extends React.Component {
    constructor(props) {
        super(props);


        this.state = { products: {}, perPage: 4, page: 1 };
        this.setPage = this.setPage.bind(this)
    }
    renderProduct(aProduct, key) {
        return (
            <Product product={aProduct} key={key} />
        )
    }
    setPage(page){
        this.setState({page})
    }
    renderProducts(pages, perPage) {
        var arr = [];
        if (this.props.producsts === {}) return;
        for (let i = (pages - 1) * perPage; i < pages * perPage; i++) {
            if(this.props.products[i] === undefined) continue;
            arr.push(this.renderProduct(this.props.products[i], i));
        }
        if (arr.length !== 0) return arr;
        else {
            return (
                <h1>Brak pasujacych wynikow</h1>
            )
        }
    }
    render() {

        return (
            <div className="productsContainer">
                {this.renderProducts(this.state.page, this.state.perPage)}
                <Pages setPage={this.setPage} pages={this.props.pages} />
            </div>
        )
    }
}
