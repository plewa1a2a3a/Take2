import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin'


const config = {
  entry: {
    js: ['./src/app-client.js', './sass/style.scss']
  },
  output: {
    path: path.join(__dirname, 'src', 'static', 'js'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'src')
        ],
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/, use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'],
          publicPath: './src/static/css'
        })
      },
      {
        test: path.join(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
          options: 'cacheDirectory=.babel_cache',
        },
      }
    ],
  },
  plugins: [
    new ExtractTextPlugin(
      '../css/compiled-style.css'
    )
  ],
};

export default config;
